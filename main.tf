provider "google" {
  project = "${ var.project_name }"
  credentials = "${file("JSON_CREDENTIALFILE_REPLACE_HERE")}"
  region  = "${ var.project_region }"
  zone    = "${ var.project_zone }"
}